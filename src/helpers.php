<?php

use ApiServer\Core\Models\Option;

/*
function getOption($key, $scope = null, $scopeId = null) {
    $query = Option::query();
    $query = $query->where('key', $key);

    if(!is_null($scope)) {
        $query = $query->where('scope', $scope);
    } else {
        $query = $query->whereNull('scope');
    }

    if(!is_null($scopeId)) {
        $query = $query->where('scope_id', $scopeId);
    } else {
        $query = $query->whereNull('scope_id');
    }
    return $query->firstOrFail()->value;
}
*/

function add_model_relation($modelClass, $relationName, $relationClosure) {
    if(!class_exists($modelClass)) {
        throw new \Exception("{$modelClass} does not exist.");
    } elseif( !is_object($relationClosure)
        || !($relationClosure instanceof \Closure)
    ) {
        throw new \Exception("Not a closure");
    }

    config([
        "modelRelations.{$modelClass}.{$relationName}" => $relationClosure
    ]);
}

function get_model_relation($modelClass, $relationName) {
    $relationClosure = config("modelRelations.{$modelClass}.{$relationName}");

    if( !is_object($relationClosure)
        || !($relationClosure instanceof \Closure)
    ) {
        throw new \BadMethodCallException(
            "Runtime method {$relationName} on class "
            ."{$modelClass} not found."
        );
    }

    return $relationClosure;
}

function add_policy_action($policyClass, $actionName, $actionClosure) {
    if(!class_exists($policyClass)) {
        throw new \Exception("{$policyClass} does not exist.");
    } elseif( !is_object($actionClosure)
        || !($actionClosure instanceof \Closure)
    ) {
        throw new \Exception("Not a closure");
    }

    config([
        "policyActions.{$policyClass}.{$actionName}" => $actionClosure
    ]);
}

function get_policy_action($policyClass, $actionName) {
    $actionClosure = config("policyActions.{$policyClass}.{$actionName}");

    if( !is_object($actionClosure)
        || !($actionClosure instanceof \Closure)
    ) {
        throw new \BadMethodCallException(
            "Runtime policy action {$actionName} on class "
            ."{$policyClass} not found."
        );
    }

    return $actionClosure;
}
