<?php

namespace ApiServer\Core\Models;

use ApiServer\Core\Models\BaseModel;
use ApiServer\Core\Models\User;

class Role extends BaseModel
{
    /**
    * Bootstrap any application services.
    */
    public static function boot()
    {
      parent::boot();

      //Register validation service
      self::creating(
          function ($model) {
              return $model->validate(['name' => '']);
          }
      );

      self::updating(
          function ($model) {
              return $model->validate(['name' => $model->name]);
          }
      );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
            'name' => 'required|between:1,100|unique:roles,name,{name},name',
            'description' => '',
    ];

    public function users() {
        return $this->belongsToMany(User::class, 'role_user', 'role_id', 'user_id');
    }

    public function permissions() {
        return $this->hasMany(Permission::class, 'role_id');
    }
}
