<?php

namespace ApiServer\Core\Models;

use Illuminate\Database\Eloquent\Model;

use ApiServer\Core\Models\BaseModel;

/**
 * Netmon\Server\Models\Permission
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\ApiServer\Core\Models\Role[] $roles
 */
class Option extends BaseModel
{
  /**
   * Bootstrap any application services.
   */
  public static function boot()
  {
      parent::boot();

      //Register validation service
      //on saving event
      self::saving(
          function ($model) {
              return $model->validate();
          }
      );
  }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'key', 'value', 'scope', 'scope_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
            'key' => 'required|alpha_dash|between:1,100',
            'value' => 'required',
            'scope' => 'nullable|alpha_dash|between:1,100',
            //TODO: add UUID validation rule. Should be introduced in Laravel 5.5
            'scope_id' => 'nullable',
    ];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [];
}
