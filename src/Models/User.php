<?php

namespace ApiServer\Core\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Tymon\JWTAuth\Contracts\JWTSubject;

use ApiServer\Core\Models\Role;
use ApiServer\Core\Models\Permission;
use ApiServer\Core\Models\BaseModel;

class User extends BaseModel implements AuthenticatableContract,
                                        CanResetPasswordContract,
                                        JWTSubject
{
    use Authenticatable, CanResetPassword;

    public function getJWTIdentifier()
    {
        return $this->id;
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot()
    {
        parent::boot();

        //Register validation service on saving event
        self::saving(
            function ($model) {
                return $model->validate();
            }
        );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [];

    /**
     * Holds the validation errors for password if
     * some before hashing
     *
     * @var boolean or Illuminate\Support\MessageBag
     */
    protected $passwordValidationErrors = false;

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
            'name' => 'required|alpha_dash|between:1,100|unique:users,name,{id}',
            'email' => 'nullable|email',
            //validate the hash (not the password!)
            //bcrypt hash must have 60 chars
            //validation of the password attribute is already done in
            //the setpasswordAttribute method
            'password' => '',
//            'password' => 'required|size:60',
            'remember_token' => '',
    ];

    /**
     * Validate the model before storing to database.
     * This method also handles password validation
     * @return boolean
     */
    public function isValid($uniqueProperties = [])
    {
        $validation = \Validator::make(
            $this->attributes,
            array_map(
                //replace placeholders with their real value
                function ($r) {
                    return str_replace('{id}', $this->id ?: '', $r);
                },
                $this->validationRules
            )
        );

        //check if model validation or password validation fails
        if ($validation->fails() || $this->passwordValidationErrors) {
            $this->validationErrors = $validation->errors();
            if ($this->passwordValidationErrors) {
                $this->validationErrors->merge($this->passwordValidationErrors);
            }
            return false;
        }

        return true;
    }

    /**
     * Set the password and validate before setting
     * because on store() the password will already
     * be hashed.
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        // passwords hashed with bcrypt should have at least 8 and at
        // most 56 bytes in length, http://en.wikipedia.org/wiki/Bcrypt
        $validation = \Validator::make(
            array('password' => $password),
            array('password' => 'required|min:8|max:56')
        );

        if ($validation->fails()) {
            $this->passwordValidationErrors = $validation->errors();
        }
        else {
            $this->attributes['password'] = \Hash::make($password);
        }
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    /**
     * $role can be
     *   - object of a role (has user this role?)
     */
    public function hasRole(Role $role)
    {
        return (bool)$this->roles->contains($role->id);
    }

     /**
      * $role can be
      *   - collection containing multiple roles (has one of these roles?)
      */
    public function hasOneOfTheseRoles($roles) {
        return (bool)$roles->intersect($this->roles)->count();
    }

    public function attach(Model $model) {
        if($model instanceof Role) {
            return $this->roles()->attach($model);
        }
        throw new \InvalidArgumentException;
    }

    public function detach(Model $model) {
        if($model instanceof Role) {
            return $this->roles()->detach($model);
        }
        throw new \InvalidArgumentException;
    }

    public function permissions() {
        return $this->hasMany(Permission::class);
    }
}
