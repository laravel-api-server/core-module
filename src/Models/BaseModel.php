<?php

namespace ApiServer\Core\Models;

use Illuminate\Database\Eloquent\Model;

use ApiServer\Core\Exceptions\Exceptions\ValidationException;
use ApiServer\Core\Contracts\ValidationContract;
use ApiServer\Core\Traits\UuidForKeyTrait;


abstract class BaseModel extends Model implements ValidationContract {
    use UuidForKeyTrait;

	protected $validator;

	public function __call($method, $parameters) {
        try {
            $runtimRelationClosure = get_model_relation(static::class, $method);
        } catch(\BadMethodCallException $e) {
            // if runtime relation does not exist, then call method of parent
            // class
	  	    return parent::__call($method, $parameters);
        }
        return $runtimRelationClosure($this);
	}

	public function getAttribute($key) {
        // if attribute can be received using the default process then do this
	  	if ($return = parent::getAttribute($key)) {
	    	return $return;
	  	}

        // if attribute cannot be received using the default process then
        // try to resolve as relationship
	  	$camelKey = camel_case($key);
        try {
            $runtimeRelation = get_model_relation(static::class, $camelKey);
        } catch(\BadMethodCallException $e) {
            // if runtime relation dos not exist, then return
            return;
        }

        return $this->getRelationshipFromMethod($key, $camelKey);
	}

	public function validationRules() {
		return $this->validationRules;
	}

	public function validationErrors() {
		return $this->validationErrors;
	}

	public function isValid($uniqueProperties = []) {
		$this->validator = \Validator::make(
				$this->attributes,
				array_map(
						//replace placeholders with their real value
						function($r) use ($uniqueProperties) {
						    try {
                                if(empty($uniqueProperties )) {
                                    return str_replace('{id}', $this->id ?: '', $r);
                                }
                            } catch(\Exception $e) {
                                \Log::info($uniqueProperties);
                                throw $e;
                            }

							foreach($uniqueProperties as $property=>$value) {
								$r = str_replace('{'.$property.'}', $value ?: '', $r);
							}
							return $r;
						},
						$this->validationRules
						)
				);

		//check if model validation or password validation fails
		if ($this->validator->fails()) {
			$this->validationErrors = $this->validator->errors();
			return false;
		}

		return true;
	}

	public function validate($uniqueProperties = []) {
		if(!$this->isValid($uniqueProperties)) {
			throw new ValidationException(null, $this->validationErrors);
		}
	}
}
