<?php

namespace ApiServer\Core\Models;

use ApiServer\Core\Models\BaseModel;
use ApiServer\Core\Models\User;

/**
 * Netmon\Server\Models\Permission
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\ApiServer\Core\Models\Role[] $roles
 */
class Permission extends BaseModel
{
  /**
   * Bootstrap any application services.
   */
  public static function boot()
  {
      parent::boot();

      //Register validation service
      //on saving event
      self::saving(
          function ($model) {
              return $model->validate();
          }
      );
  }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'user_id', 'role_id', 'action_id',
      'resource_id', 'object_key'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
            'user_id' => 'required_without:role_id|exists:users,id',
            'role_id' => 'required_without:user_id|exists:roles,id',
            'action_id' => 'required|between:1,100',
            'resource_id' => 'required|between:1,100|unique_with:permissions,user_id,role_id,action_id,object_key',
            'object_key' => '',
    ];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [];

    public function user() {
      return $this->belongsTo(User::class);
    }

    public function role() {
    	return $this->belongsTo(Role::class);
    }
}
