<?php

namespace ApiServer\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use ApiServer\Core\Policies\BasePolicy;
use ApiServer\Core\Models\User;

class UserPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $authUser) {
        //everyone is allowed to view the user list
        return true;
    }

    public function store(User $authUser, User $user = null) {
        //default permissions
        return $this->checkPermissions($authUser, 'store', 'user');
    }

    public function show(User $authUser, User $user) {
        //everyone is allowed to show a user
        return true;
    }

    public function update(User $authUser, User $user) {
        //a user is always allowed to edit himself
        if($authUser->id === $user->id) {
            return true;
        }

        //default permissions
        return $this->checkPermissions($authUser, 'update', 'user', $user);
    }

    public function destroy(User $authUser, User $user) {
        //a user is allowed to delete himself
        if($authUser->id == $user->id) {
            return true;
        }

        //default permissions
        return $this->checkPermissions($authUser, 'destroy', 'user', $user);
    }
}
