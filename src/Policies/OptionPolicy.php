<?php

namespace ApiServer\Core\Policies;

use ApiServer\Core\Policies\BasePolicy;
use ApiServer\Core\Models\User;
use ApiServer\Core\Models\Option;

class OptionPolicy extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $authUser) {
      return $this->checkPermissions($authUser, 'index', 'option');
    }

    public function store(User $authUser, Option $option = null) {
      return $this->checkPermissions($authUser, 'store', 'option');
    }

    public function show(User $authUser, Option $option) {
      return $this->checkPermissions($authUser, 'show', 'option', $option);
    }

    public function update(User $authUser, Option $option) {
      return $this->checkPermissions($authUser, 'update', 'option', $option);
    }

    public function destroy(User $authUser, Option $option) {
      return $this->checkPermissions($authUser, 'destroy', 'option', $option);
    }
}
