<?php

namespace ApiServer\Core\Policies;

use ApiServer\Core\Models\User;
use ApiServer\Core\Models\Role;

class RolePolicy extends BasePolicy
{
    public function index(User $authUser) {
      return $this->checkPermissions($authUser, 'index', 'role');
    }

    public function store(User $authUser, Role $role = null) {
      return $this->checkPermissions($authUser, 'store', 'role');
    }

    public function show(User $authUser, Role $role) {
      return $this->checkPermissions($authUser, 'show', 'role', $role);
    }

    public function update(User $authUser, Role $role) {
      return $this->checkPermissions($authUser, 'update', 'role', $role);
    }

    public function destroy(User $authUser, Role $role) {
      return $this->checkPermissions($authUser, 'destroy', 'role', $role);
    }
}
