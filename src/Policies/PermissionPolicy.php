<?php

namespace ApiServer\Core\Policies;

use ApiServer\Core\Models\User;
use ApiServer\Core\Models\Permission;

class PermissionPolicy extends BasePolicy
{
    public function index(User $authUser) {
      return $this->checkPermissions($authUser, 'index', 'permission');
    }

    public function store(User $authUser, Permission $permission = null) {
      return $this->checkPermissions($authUser, 'store', 'permission');
    }

    public function show(User $authUser, Permission $permission) {
      return $this->checkPermissions($authUser, 'show', 'permission', $permission);
    }

    public function update(User $authUser, Permission $permission) {
      return $this->checkPermissions($authUser, 'update', 'permission', $permission);
    }

    public function destroy(User $authUser, Permission $permission) {
      return $this->checkPermissions($authUser, 'destroy', 'permission', $permission);
    }
}
