<?php

namespace ApiServer\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use ApiServer\Core\Models\User;
use ApiServer\Core\Models\Role;
use ApiServer\Core\Models\Permission;


abstract class BasePolicy
{
    use HandlesAuthorization;

    public function __call($method, $parameters) {
        try {
            $runtimePolicyActionClosure = get_policy_action(static::class, $method);
        } catch(\BadMethodCallException $e) {
            // if runtime relation does not exist, then call method of parent
            // class
            // return parent::__call($method, $parameters);

            // this class has no parent class, so just throw the exception
            throw $e;
        }
        return $runtimePolicyActionClosure($this, $parameters);
    }

    public static function checkPermissions(User $authUser, $actionId, $resourceId, $object=null) {
        //if we do not have a valid authenticated user return false
        if(!isset($authUser->id))
            return false;

        //get permissions that are assigned to roles
        $rolePermissions = Permission::with(['role'])
                                    ->whereNotNull('role_id')
                                    ->where('action_id', '=', $actionId)
                                    ->where('resource_id', '=', $resourceId);
        if(is_null($object)) {
            $rolePermissions->whereNull('object_key');
        } else {
            $rolePermissions->where(function ($query) use ($object) {
                $query->where('object_key', '=', $object->getKey())
                        ->orWhereNull('object_key');
            });
        }
        $rolePermissions = $rolePermissions->get();

        //Is the user assigned to a role that has the requested permission?
        foreach($rolePermissions as $permission) {
            if(!is_null($permission->role) && $authUser->hasRole($permission->role)) {
                return true;
            }
        }

        //get permissions that are assigned to a user
        $userPermissions = Permission::where('user_id', '=', $authUser->id)
                                    ->where('action_id', '=', $actionId)
                                    ->where('resource_id', '=', $resourceId);
        if(is_null($object)) {
            $userPermissions->whereNull('object_key');
        } else {
            $userPermissions->where(function($query) use ($object) {
                $query->where('object_key', '=', $object->getKey())
                    ->orWhereNull('object_key');
            });
        }
        $userPermissions = $userPermissions->get();
        //check if user has "store role" permission
        if((bool)$userPermissions->count()) {
            return true;
        }

        return false;
    }
}
