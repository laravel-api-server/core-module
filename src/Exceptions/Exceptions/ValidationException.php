<?php

namespace ApiServer\Core\Exceptions\Exceptions;

use Illuminate\Support\MessageBag;

class ValidationException extends \Exception
{
    protected $code = 422;
    protected $errors;

    public function __construct (
        $message = "Validation failed.",
        MessageBag $errors,
        \Exception $previous = NULL
    ) {
        parent::__construct($message, $this->code, $previous);
        $this->errors = $errors;
    }

    public function getValidationErrors() {
        return $this->getErrors();
    }

    public function getErrors() {
        return $this->errors;
    }

    public function __toString() {
        return $this->errors->__toString();
    }
}
