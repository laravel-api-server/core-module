<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use ApiServer\Core\Models\Role;
use ApiServer\Core\Models\Resource;
use ApiServer\Core\Models\Option;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->string('name', 100)->unique();
          $table->text('description')->nullable();
          $table->timestamps();
        });

        $adminRole = Role::create(['name' => "Admin"]);
        Option::create([
            'key' => "serverAdminRoleId",
            'value' => $adminRole->id
        ]);
        $userRole = Role::create(['name' => "User"]);
        Option::create([
            'key' => "serverUserRoleId",
            'value' => $userRole->id
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Option::where('key', '=', 'serverUserRoleId')->delete();
        Option::where('key', '=', 'serverAdminRoleId')->delete();
        Schema::drop('roles');
    }
}
