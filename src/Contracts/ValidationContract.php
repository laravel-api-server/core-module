<?php

namespace ApiServer\Core\Contracts;

interface ValidationContract {
	/**
	 * Returns an array containing the validation
	 * rules
	 */
	public function validationRules();

	/**
	 * Returns an array containing the falidation
	 * errors if some
	 */
	public function validationErrors();

	/**
	 * Checks if a model implementing this contract
	 * is valid and returns true or false.
	 */
	public function isValid();

	/**
	 * Checks if a model implementing this contract
	 * is valid and throws a ValidationException
	 * if model is not valid
	 */
	public function validate();

}
