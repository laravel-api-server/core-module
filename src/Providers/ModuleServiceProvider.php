<?php

namespace ApiServer\Core\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function register()
    {
        //register service provider for this module
        $this->app->register('ApiServer\Core\Providers\AuthServiceProvider');
        $this->app->register('ApiServer\Core\Providers\GateServiceProvider');
        $this->app->register('ApiServer\ErrorHandler\Providers\ModuleServiceProvider');

        // register JWT service providers and facades
        $this->app->register('Tymon\JWTAuth\Providers\LaravelServiceProvider');
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('JWTAuth', 'Tymon\JWTAuth\Facades\JWTAuth');
        $loader->alias('JWTFactory', 'Tymon\JWTAuth\Facades\JWTFactory');
    }

    public function boot()
    {
        //register database migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
}

?>
