<?php

namespace ApiServer\Core\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'ApiServer\Core\Models\Option' => 'ApiServer\Core\Policies\OptionPolicy',
        'ApiServer\Core\Models\User' => 'ApiServer\Core\Policies\UserPolicy',
        'ApiServer\Core\Models\Role' => 'ApiServer\Core\Policies\RolePolicy',
        'ApiServer\Core\Models\Permission' => 'ApiServer\Core\Policies\PermissionPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
