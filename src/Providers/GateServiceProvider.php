<?php

namespace ApiServer\Core\Providers;

use Illuminate\Auth\AuthServiceProvider as BaseAuthServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

use ApiServer\Core\Auth\Access\Gate;
use ApiServer\Core\Models\User;

class GateServiceProvider extends BaseAuthServiceProvider
{
    /**
     * Register the access gate service.
     *
     * @return void
     */
    protected function registerAccessGate()
    {
        // This does two things:
        // 1. register our custom gate with some additional methods for
        //    accessing internal data (like the array of registered policies)
        // 2. Change gates user resolver. By default Gate uses
        //    $app['auth']->userResolver() which will return null if the user
        //    is not authenticated. See registerAccessGate()-method in
        //    https://github.com/laravel/framework/blob/5.4/src/Illuminate/Auth/AuthServiceProvider.php
        //    Returning null for the user will make all policies immediately
        //    fail for all guest users. See raw()-method in
        //    https://github.com/laravel/framework/blob/5.4/src/Illuminate/Auth/Access/Gate.php
        //    To allow checking of gate policies for guest users we add a custom
        //    user resolver closure that returns an empty user for guests.
        //    See https://stackoverflow.com/a/38599452/6517870

        $this->app->singleton(GateContract::class, function ($app) {
            return new Gate($app, function () use ($app) {
                //return call_user_func($app['auth']->userResolver());

                $user = $app['auth']->user();
                if ($user) {
                    return $user;
                }
                return new User;
            });
        });
    }
}
