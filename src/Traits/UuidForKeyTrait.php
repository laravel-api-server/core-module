<?php

//Credits: https://garrettstjohn.com/article/using-uuids-laravel-eloquent-orm/

namespace ApiServer\Core\Traits;

use Ramsey\Uuid\Uuid;

trait UuidForKeyTrait
{
    /**
     * Boot the Uuid trait for the model.
     * https://laravel-news.com/2015/06/booting-eloquent-model-traits/
     *
     * @return void
     */
    public static function bootUuidForKeyTrait()
    {
        static::creating(function ($model) {
            $model->incrementing = false;
            $model->{$model->getKeyName()} = (string)Uuid::uuid4();
        });
    }

    /**
     * Get the casts array.
     *
     * @return array
     */
    public function getCasts()
    {
        return $this->casts;
    }
}
