<?php

namespace ApiServer\Core\Auth\Access;

use Illuminate\Auth\Access\Gate as BaseGate;

class Gate extends BaseGate {
    public function getPolicyMappings() {
        return $this->policies;
    }
}

?>
