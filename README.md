# Users Module for the API-Server
This project holds the Users module for the API-Server. It supports the modularization of the server.

## Installation
Add the modules service provider to the `providers` array of your `configs/app.php`:
```
        ApiServer\Core\ModuleServiceProvider::class,
```

Adjust the User model of your authentication system by adjusting the `providers` attribute
in your `config/auth.php`:
```
'providers' => [
    'users' => [
        'driver' => 'eloquent',
        'model' => ApiServer\Core\Models\User',
    ],
],
```

## Tmp
```
ApiServer\Exceptions\Exceptions\Exceptions\UnauthorizedException
ApiServer\Authentication\Exceptions\Exceptions\UnauthorizedException

ApiServer\Exceptions\Exceptions\Exceptions\ValidationException
ApiServer\Core\Exceptions\Exceptions\ValidationException

ApiServer\Base
ApiServer\Core

ApiServer\Users
ApiServer\Core

ApiServer\Authorization
ApiServer\Core

ApiServer\Configs
ApiServer\Core

ApiServer\Core\Models\Config
ApiServer\Core\Models\Option

ApiServer\Core\Policies\ConfigPolicy
ApiServer\Core\Policies\OptionPolicy

Config::
Option::

new Config
new Option

Config
Option
```

## Depencies
 * Authorization
 * Option
 * ApiServer\JsonApi
 * tymon/jwt-auth

## Provides
Nothing that is worth to mention.

## Documentation
 * [Technology stack](doc/technology.md)
 * [Unittesting](doc/unittesting.md)
 * [Creating a release](doc/release.md)

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

# License
See [License](LICENSE.txt)
